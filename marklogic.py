#! /usr/bin/env python

import requests
from requests.auth import HTTPDigestAuth
import json

class MarkLogic(object):
    def __init__(self, hostname, username, password):
        self.manage_base_url = 'http://' + hostname + ':8002/manage/v2/'
        self.auth = HTTPDigestAuth(username,password)

    def clusters(self):
        r = requests.get(self.manage_base_url + 'clusters?format=json',  auth=self.auth)
        if r.status_code != requests.codes.ok:
            print r.text
        r.raise_for_status()
        if 'list-item' in r.json()['cluster-default-list']['list-items']:
            return r.json()['cluster-default-list']['list-items']['list-item']
        else:
            return []
    
    def local_cluster(self):
        r = requests.get(self.manage_base_url + 'clusters?cluster-role=local&format=json',  auth=self.auth)
        if r.status_code != requests.codes.ok:
            print r.text
        r.raise_for_status()
        if 'list-item' in r.json()['cluster-default-list']['list-items']:
            return r.json()['cluster-default-list']['list-items']['list-item'][0]['nameref']
        else:
            return ""
    
    def get_cluster_properties(self):
        r = requests.get(self.manage_base_url + 'properties?format=json',  auth=self.auth)
        if r.status_code != requests.codes.ok:
            print r.text
        r.raise_for_status()
        return r.json()

    def add_cluster(self, data):
        r = requests.post(self.manage_base_url + 'clusters',  auth=self.auth, headers = {'content-type': 'application/json'}, data = json.dumps(data))
        if r.status_code != requests.codes.ok:
            print r.text
        r.raise_for_status()

    def delete_cluster(self, cluster_name):
        r = requests.delete(self.manage_base_url + 'clusters/' + cluster_name,  auth=self.auth)
        if r.status_code != requests.codes.ok:
            print r.text
        r.raise_for_status()

    def set_master(self, master_cluster, database):
        data = { "operation": "set-foreign-master", "foreign-master": { "foreign-cluster-name": master_cluster, "foreign-database-name": database, "connect-forests-by-name": True}} 
        r = requests.post(self.manage_base_url + 'databases/' + database,  auth=self.auth, headers = {'content-type': 'application/json'}, data = json.dumps(data))
        if r.status_code != requests.codes.ok:
            print r.text
        r.raise_for_status()

    def set_replica(self, replica_cluster, database):
        data = { "operation": "add-foreign-replicas", "foreign-replica": [{ "foreign-cluster-name": replica_cluster, "foreign-database-name": database, "connect-forests-by-name": True, "lag-limit": 23, "enabled": True}]}
        r = requests.post(self.manage_base_url + 'databases/' + database,  auth=self.auth, headers = {'content-type': 'application/json'}, data = json.dumps(data))
        if r.status_code != requests.codes.ok:
            print r.text
        r.raise_for_status()

    def remove_master(self, database):
        data = { "operation": "remove-foreign-master" }
        r = requests.post(self.manage_base_url + 'databases/' + database,  auth=self.auth, headers = {'content-type': 'application/json'}, data = json.dumps(data))
        if r.status_code != requests.codes.ok:
            print r.text
        r.raise_for_status()

    def remove_replica(self, database):
        data = { "operation": "remove-foreign-replicas", "foreign-database-name": [ database ] } 
        r = requests.post(self.manage_base_url + 'databases/' + database,  auth=self.auth, headers = {'content-type': 'application/json'}, data = json.dumps(data))
        if r.status_code != requests.codes.ok:
            print r.text
        r.raise_for_status()

    def replication_status(self, database):
        r = requests.get(self.manage_base_url + 'databases/' + database + '?view=status&format=json',  auth=self.auth)
        if r.status_code != requests.codes.ok:
            print r.text
        r.raise_for_status()
        return r.json()['database-status']['status-properties']['database-replication-status']


