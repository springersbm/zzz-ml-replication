#!/bin/bash

curlMaster="curl -fsS --anyauth -u ${MASTER_ADMIN_USER}:${MASTER_ADMIN_PASSWORD} http://${masterClusterBootstrapHost}:7655/ml-replication/v1"
curlSlave="curl -fsS --anyauth -u ${SLAVE_ADMIN_USER}:${SLAVE_ADMIN_PASSWORD} http://${slaveClusterBootstrapHost}:7655/ml-replication/v1"

authMaster="username=${MASTER_ADMIN_USER}&password=${MASTER_ADMIN_PASSWORD}"
authSlave="username=${SLAVE_ADMIN_USER}&password=${SLAVE_ADMIN_PASSWORD}"

coupleClusters() {
  echo "Coupling clusters ${masterClusterName} and ${slaveClusterName} ..."
  ${curlMaster}"/add-foreign-cluster.xqy?hostname=${slaveClusterBootstrapHost}&${authSlave}"
  ${curlSlave}"/add-foreign-cluster.xqy?hostname=${masterClusterBootstrapHost}&${authMaster}"
}

configureReplication() {
  IFS=',' read -a databaseArray <<< "$replicatedDatabases"
  for database in "${databaseArray[@]}"
  do
    echo "Configuring replication for ${database} ..."
    ${curlMaster}"/set-foreign-replicas.xqy?cluster=${slaveClusterName}&${authSlave}&database=${database}"
    ${curlSlave}"/set-foreign-master.xqy?cluster=${masterClusterName}&${authMaster}&database=${database}"
  done
}

disableReplication() {
  IFS=',' read -a databaseArray <<< "$replicatedDatabases"
  for database in "${databaseArray[@]}"
  do
    echo "Disabling replication for ${database} ..."
    ${curlMaster}"/set-foreign-replicas.xqy?cluster=${slaveClusterName}&${authSlave}&database=${database}&replication-enabled=false"
  done
}

removeReplication() {
  IFS=',' read -a databaseArray <<< "$replicatedDatabases"
  for database in "${databaseArray[@]}"
  do
    echo "Deleting replication config for database ${database} ..."
    ${curlMaster}"/remove.xqy?database=${database}&foreign-cluster-name=${slaveClusterName}"
    ${curlSlave}"/remove.xqy?database=${database}&foreign-cluster-name=${masterClusterName}"
  done
}

getReplicationConfig() {
  ${curlMaster}"/status.xqy"
}
