#!/bin/bash
set -e

target_host=${1-"ml.local.springer-sbm.com"}
file=${2-"target/ml-replication-modules_v1.zip"}

deploy_url="http://${target_host}:7654/apps/ml-replication/v1"

echo "Deleting existing ml-replication at ${target_host}"
curl -fsS --digest -u deployer:DeployMe -X DELETE "$deploy_url"
echo
echo "Installing new ml-replication from ${file}"
curl -fsS --digest -u deployer:DeployMe --upload-file "$file" "$deploy_url"
echo
echo "Done"
