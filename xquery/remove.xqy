xquery version '1.0-ml';

import module namespace admin = "http://marklogic.com/xdmp/admin" at "/MarkLogic/admin.xqy";
import module namespace replication = "replication" at "replication/replication.xqy";
import module namespace output = "output" at "replication/output.xqy";
declare option xdmp:output "method=text";

declare variable $database-name as xs:string := xdmp:get-request-field("database");
declare variable $foreign-cluster-name as xs:string := xdmp:get-request-field("foreign-cluster-name");

let $local-database-id := admin:database-get-id(admin:get-configuration(), $database-name)

return (
  replication:delete-foreign-replicas($local-database-id, $foreign-cluster-name),
  replication:delete-foreign-master($local-database-id),
  output:log(fn:concat("Deleted replication config for database ", $database-name, ", cluster ", $foreign-cluster-name))
)
