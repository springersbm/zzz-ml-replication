xquery version "1.0-ml";

import module namespace admin = "http://marklogic.com/xdmp/admin" at "/MarkLogic/admin.xqy";
import module namespace output = "output" at "replication/output.xqy";

declare option xdmp:output "method=text";

declare variable $name := xdmp:get-request-field("name", "");

if ($name ne "") then
  let $cfg := admin:get-configuration()
  let $cfg := admin:cluster-set-name($cfg, $name)
  return (
    admin:save-configuration($cfg),
    output:log(fn:concat("Cluster name set to ", $name))
  )
else (
  xdmp:set-response-code(400, "parameter 'name' missing"),
  output:log("Error: parameter 'name' missing")
)
