xquery version '1.0-ml';

import module namespace admin = "http://marklogic.com/xdmp/admin" at "/MarkLogic/admin.xqy";
import module namespace output = "output" at "replication/output.xqy";

declare default function namespace "local";
declare namespace clusters = "http://marklogic.com/manage/clusters";
declare namespace hosts = "http://marklogic.com/manage/hosts";

declare option xdmp:output "method=text";

declare variable $hostname as xs:string := xdmp:get-request-field("hostname");
declare variable $username as xs:string := xdmp:get-request-field("username");
declare variable $password as xs:string := xdmp:get-request-field("password");

declare variable $http-options :=
  <options xmlns="xdmp:http">
    <authentication method="digest">
      <username>{$username}</username>
      <password>{$password}</password>
    </authentication>
  </options>;

declare function get-foreign-cluster($hostname) {
  let $foreign-clusters-xml := xdmp:http-get(
    fn:concat("http://",$hostname, ":8002/manage/v2/clusters?format=xml&amp;cluster-role=local"),
    $http-options
  )
  let $foreign-cluster-id := fn:string($foreign-clusters-xml//clusters:list-item[1]/clusters:idref)
  let $foreign-cluster-name := fn:string($foreign-clusters-xml//clusters:list-item[1]/clusters:nameref)
  return
    <cluster>
      <id>{$foreign-cluster-id}</id>
      <name>{$foreign-cluster-name}</name>
    </cluster>
};


declare function get-host($hostname) {
  let $foreign-hosts-xml := xdmp:http-get(
    fn:concat("http://",$hostname, ":8002/manage/v2/hosts?format=xml"),
    $http-options
  )
  let $foreign-host-id := fn:string($foreign-hosts-xml//hosts:list-item[1]/hosts:idref)
  let $foreign-host-name := fn:string($foreign-hosts-xml//hosts:list-item[1]/hosts:nameref)
  return
    <host>
      <id>{$foreign-host-id}</id>
      <name>{$foreign-host-name}</name>
    </host>
};

try {
    let $foreign-cluster := get-foreign-cluster($hostname)
    let $foreign-host := get-host($hostname)
    let $bootstrap-host := admin:foreign-host($foreign-host/id, $foreign-host/name, 7998)

    return
        if (fn:index-of(admin:cluster-get-foreign-cluster-ids(admin:get-configuration()), $foreign-cluster/id))
        then
            output:log(fn:concat("Foreign cluster ", $foreign-cluster/name, " already exists."))
        else
            let $updated-cfg := admin:foreign-cluster-create(admin:get-configuration(), $foreign-cluster/id, $foreign-cluster/name, (), (), (), (), (), (), (), ($bootstrap-host))
            return (
                admin:save-configuration($updated-cfg),
                output:log(fn:concat("Created foreign cluster ", $foreign-cluster/name, "."))
            )
} catch ($exception) {
    xdmp:set-response-code(503,fn:concat("ERROR: ", fn:string-join($exception//*:datum, ". ")))
}
