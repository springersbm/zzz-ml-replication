xquery version '1.0-ml';
import module namespace admin = "http://marklogic.com/xdmp/admin" at "/MarkLogic/admin.xqy";
import module namespace output = "output" at "replication/output.xqy";

declare option xdmp:output "method=text";

declare variable $config := admin:get-configuration();

let $output :=
  for $dbid in admin:get-database-ids($config)
  for $foreign-replica in admin:database-get-foreign-replicas($config, $dbid)
  return
    fn:concat(
      "database=", admin:database-get-name($config, $dbid),
      " cluster=", admin:foreign-cluster-get-name($config, $foreign-replica/*:foreign-cluster-id),
      " enabled=", $foreign-replica/*:replication-enabled/fn:string()
    )

return
  if ($output) then output:log($output)
  else output:log("No databases configured for replication.")
