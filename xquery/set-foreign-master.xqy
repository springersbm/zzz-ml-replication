xquery version '1.0-ml';

import module namespace admin = "http://marklogic.com/xdmp/admin" at "/MarkLogic/admin.xqy";
import module namespace replication = "replication" at "replication/replication.xqy";
import module namespace output = "output" at "replication/output.xqy";

declare option xdmp:output "method=text";

declare variable $cluster-name as xs:string := xdmp:get-request-field("cluster");
declare variable $username  as xs:string := xdmp:get-request-field("username");
declare variable $password  as xs:string := xdmp:get-request-field("password");
declare variable $database-name  as xs:string := xdmp:get-request-field("database");

let $foreign-cluster-id := admin:cluster-get-foreign-cluster-id(admin:get-configuration(), $cluster-name)
let $foreign-host-name := admin:foreign-cluster-get-bootstrap-hosts(admin:get-configuration(), $foreign-cluster-id)[1]//*:foreign-host-name/fn:string()
let $local-database-id := admin:database-get-id(admin:get-configuration(), $database-name)
let $foreign-database-id := replication:get-foreign-database-id($database-name, $foreign-host-name, $username, $password)

(: A database cannot be both master and replica, so delete foreign-replicas config if it exists. :)
let $dummy := replication:delete-foreign-replicas($local-database-id)

let $foreign-master := admin:database-foreign-master($foreign-cluster-id, $foreign-database-id, fn:true())
let $updated-cfg := admin:database-set-foreign-master(admin:get-configuration(), $local-database-id, $foreign-master)
return (
  admin:save-configuration($updated-cfg),
  output:log(
    fn:concat("Set foreign master for database ", $database-name, " from cluster ", $cluster-name)
  )
)
