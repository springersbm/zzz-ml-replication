xquery version '1.0-ml';

import module namespace admin = "http://marklogic.com/xdmp/admin" at "/MarkLogic/admin.xqy";
import module namespace replication = "replication" at "replication/replication.xqy";
import module namespace output = "output" at "replication/output.xqy";

declare option xdmp:output "method=text";

declare variable $cluster-name as xs:string := xdmp:get-request-field("cluster");
declare variable $username  as xs:string := xdmp:get-request-field("username");
declare variable $password  as xs:string := xdmp:get-request-field("password");
declare variable $database-name  as xs:string := xdmp:get-request-field("database");
declare variable $replication-enabled as xs:boolean := xdmp:get-request-field("replication-enabled", "true") = "true";

let $foreign-cluster-id := admin:cluster-get-foreign-cluster-id(admin:get-configuration(), $cluster-name)
let $foreign-host-name := admin:foreign-cluster-get-bootstrap-hosts(admin:get-configuration(), $foreign-cluster-id)[1]//*:foreign-host-name/fn:string()
let $local-database-id := admin:database-get-id(admin:get-configuration(), $database-name)
let $foreign-database-id := replication:get-foreign-database-id($database-name, $foreign-host-name, $username, $password)

(: A database cannot be both master and replica, so delete foreign-master config if it exists. :)
let $dummy := replication:delete-foreign-master($local-database-id)

let $existing-foreign-replicas := admin:database-get-foreign-replicas(admin:get-configuration(), $local-database-id)
let $existing-foreign-replica-for-cluster-id := $existing-foreign-replicas[*:foreign-cluster-id=$foreign-cluster-id]
let $existing-foreign-replicas-for-other-clusters := $existing-foreign-replicas[*:foreign-cluster-id!=$foreign-cluster-id]
let $new-or-updated-foreign-replica := if (exists($existing-foreign-replica-for-cluster-id))
    then admin:database-foreign-replica-set-replication-enabled($existing-foreign-replica-for-cluster-id, $replication-enabled)
    else admin:database-foreign-replica($foreign-cluster-id, $foreign-database-id, fn:true(), 15, $replication-enabled)
let $updated-cfg := admin:database-set-foreign-replicas(admin:get-configuration(),$local-database-id, ($existing-foreign-replicas-for-other-clusters, $new-or-updated-foreign-replica))

return (
  admin:save-configuration($updated-cfg),
  output:log(
    fn:concat("Set foreign replicas for database ", $database-name, " in cluster ", $cluster-name,
    ". Replication ", if ($replication-enabled) then "enabled." else "disabled.")
  )
)
