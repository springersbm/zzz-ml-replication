xquery version '1.0-ml';

module namespace replication = "replication";
import module namespace admin = "http://marklogic.com/xdmp/admin" at "/MarkLogic/admin.xqy";

declare function get-foreign-database-id($database, $foreign-host, $username, $password) {
  let $foreign-databases-xml := xdmp:http-get(
    fn:concat("http://", $foreign-host, ":8002/manage/v2/databases/", $database, "?format=xml"),
    <options xmlns="xdmp:http">
      <authentication method="digest">
          <username>{$username}</username>
          <password>{$password}</password>
      </authentication>
    </options>
  )
  return xs:unsignedLong($foreign-databases-xml//*:id/fn:string())
};

declare function delete-foreign-master($local-database-id) {
  let $config := admin:get-configuration()
  where fn:exists(admin:database-get-foreign-master($config, $local-database-id))
  return admin:save-configuration(admin:database-delete-foreign-master($config, $local-database-id))
};

declare function delete-foreign-replicas($local-database-id, $foreign-cluster-name) {
  let $config := admin:get-configuration()
  let $foreign-cluster-id := admin:cluster-get-foreign-cluster-id($config, $foreign-cluster-name)
  let $existing := admin:database-get-foreign-replicas($config, $local-database-id)
  let $config := admin:database-set-foreign-replicas($config, $local-database-id, $existing[*:foreign-cluster-id ne $foreign-cluster-id])
  return admin:save-configuration($config)
};

declare function delete-foreign-replicas($local-database-id) {
    let $updated-config := admin:database-set-foreign-replicas(admin:get-configuration(), $local-database-id, ())
    return admin:save-configuration($updated-config)
};
