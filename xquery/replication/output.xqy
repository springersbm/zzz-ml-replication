module namespace output = "output";

declare %private variable $cluster-name as xs:string := xdmp:cluster-name(xdmp:cluster());

declare function log($msg as xs:string*) {
  fn:string-join($msg ! fn:concat("[", $cluster-name, "] ", ., "&#10;"), "")
};
