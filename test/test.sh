#!/bin/bash

BIN_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${BIN_DIR}/test-functions.sh"

echo "##########################################"
echo "Configuring replication : master-slave1"
echo "##########################################"

./bin/replication enable conf/master-slave1.cfg

confirmStatusContains 'database=CasperDocuments cluster=live-slave1 enabled=true'
confirmStatusContains 'database=GenericXML cluster=live-slave1 enabled=true'

#echo "##########################################"
#echo "Configuring replication : master-slave2"
#echo "##########################################"
#
#./bin/replication enable conf/master-slave2.cfg
#confirmStatusContains 'database=CasperDocuments cluster=live-slave2 enabled=true'
#confirmStatusContains 'database=GenericXML cluster=live-slave2 enabled=true'

echo "##########################################"
echo "Disable replication master-slave1"
echo "##########################################"

./bin/replication disable conf/master-slave1.cfg
confirmStatusContains 'database=CasperDocuments cluster=live-slave1 enabled=false'
confirmStatusContains 'database=GenericXML cluster=live-slave1 enabled=false'
#confirmStatusContains 'database=CasperDocuments cluster=live-slave2 enabled=true'
#confirmStatusContains 'database=GenericXML cluster=live-slave2 enabled=true'


echo "##########################################"
echo "Re-enable replication master-slave1"
echo "##########################################"

./bin/replication enable conf/master-slave1.cfg
confirmStatusContains 'database=CasperDocuments cluster=live-slave1 enabled=true'
confirmStatusContains 'database=GenericXML cluster=live-slave1 enabled=true'
#confirmStatusContains 'database=CasperDocuments cluster=live-slave2 enabled=true'
#confirmStatusContains 'database=GenericXML cluster=live-slave2 enabled=true'

echo "##########################################"
echo "Configuring replication : master-slaveunknown"
echo "##########################################"

response=`./bin/replication enable conf/master-slaveunknown.cfg 2>&1`
if [[ "$?" != "0" ]]; then
    echo "Success: Got an error as expected."
else
    echo "Failed: Didn't get an error but should have."
fi
