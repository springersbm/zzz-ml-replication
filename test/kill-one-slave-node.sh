#!/bin/bash

BIN_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

set -e
set -u

source "${BIN_DIR}/test-functions.sh"

echo  "###################################"
echo  "Set up 2, 2-node clusters"
echo  "###################################"

upVm ml1
upVm ml2
upVm ml3
upVm ml4

echo  "###################################"
echo  "Enable replication from master cluster to slave"
echo  "###################################"

./bin/replication enable conf/master-slave1.cfg

echo  "###################################"
echo  "Start aim writing docs into master cluster"
echo  "###################################"

if [[ `aimIsRunning` == "true" ]]; then
    echo "Aim is running"
else
    echo "Aim is not running. Starting..."
    aimStart
fi

echo  "###################################"
echo  "Document counts before"
echo  "###################################"

ml1CasperDocumentsDocCountBefore=`getDocCount 'ml1.domain.com' 'CasperDocuments'`
ml3CasperDocumentsDocCountBefore=`getDocCount 'ml3.domain.com' 'CasperDocuments'`

echo "ml1CasperDocumentsDocCountBefore=$ml1CasperDocumentsDocCountBefore"
echo "ml3CasperDocumentsDocCountBefore=$ml3CasperDocumentsDocCountBefore"

echo  "###################################"
echo  "Killing one node in slave cluster"
echo  "###################################"

haltVm ml4

echo  "###################################"
echo  "Adding 2 deliveries"
echo  "###################################"

latestDelivery=`deliverNDeliveryDirs 2 | tail -1`
echo "latestDelivery=$latestDelivery"

echo  "###################################"
echo  "Waiting until Aim has picked up new deliveries..."
echo  "###################################"

waitTilAimHasProcessed "$latestDelivery"

echo  "###################################"
echo  "Check document counts on master and slave are equal"
echo  "###################################"

docCountsAreEqual=`confirmDocCountsAreEqual 'ml1.domain.com' 'ml3.domain.com' 'CasperDocuments'`
echo "docCountsAreEqual=$docCountsAreEqual"

echo  "###################################"
echo  "Document counts after"
echo  "###################################"

ml1CasperDocumentsDocCountAfter=`getDocCount 'ml1.domain.com' 'CasperDocuments'`
ml3CasperDocumentsDocCountAfter=`getDocCount 'ml3.domain.com' 'CasperDocuments'`

echo "ml1CasperDocumentsDocCountAfter=$ml1CasperDocumentsDocCountAfter"
echo "ml3CasperDocumentsDocCountAfter=$ml3CasperDocumentsDocCountAfter"

echo  "###################################"
echo  "Restarting dead node in slave cluster"
echo  "###################################"

upVm ml4

echo  "###################################"
echo  "Adding 2 deliveries"
echo  "###################################"

latestDelivery=`deliverNDeliveryDirs 2 | tail -1`
echo "latestDelivery=$latestDelivery"

echo  "###################################"
echo  "Waiting until Aim has picked up new deliveries..."
echo  "###################################"

waitTilAimHasProcessed "$latestDelivery"

echo  "###################################"
echo  "Check document counts on master and slave are equal"
echo  "###################################"

docCountsAreEqual=`confirmDocCountsAreEqual 'ml1.domain.com' 'ml3.domain.com' 'CasperDocuments'`
echo "docCountsAreEqual=$docCountsAreEqual"

echo  "###################################"
echo  "Document counts after"
echo  "###################################"

ml1CasperDocumentsDocCountAfter=`getDocCount 'ml1.domain.com' 'CasperDocuments'`
ml3CasperDocumentsDocCountAfter=`getDocCount 'ml3.domain.com' 'CasperDocuments'`

echo "ml1CasperDocumentsDocCountAfter=$ml1CasperDocumentsDocCountAfter"
echo "ml3CasperDocumentsDocCountAfter=$ml3CasperDocumentsDocCountAfter"
