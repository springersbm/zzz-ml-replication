#!/bin/bash

BIN_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

set -e
set -u

source "${BIN_DIR}/test-functions.sh"

## assume 3, 1-node clusters.
upVm ml1
upVm ml3
upVm ml5

echo "###################################"
echo "Enable replication from master cluster to slaves"
echo "###################################"
./bin/replication enable conf/master-slave1.cfg
./bin/replication enable conf/master-slave2.cfg

echo  "###################################"
echo  "Ensure Aim is running and writing to master"
echo  "###################################"
aimUpdateMarkLogicUrlHost 'ml1.domain.com'


echo  "###################################"
echo  "Document counts"
echo  "###################################"

ml1CasperDocumentsDocCountBefore=`getDocCount 'ml1.domain.com' 'CasperDocuments'`
ml3CasperDocumentsDocCountBefore=`getDocCount 'ml3.domain.com' 'CasperDocuments'`
ml5CasperDocumentsDocCountBefore=`getDocCount 'ml5.domain.com' 'CasperDocuments'`

echo "ml1CasperDocumentsDocCountBefore=$ml1CasperDocumentsDocCountBefore"
echo "ml3CasperDocumentsDocCountBefore=$ml3CasperDocumentsDocCountBefore"
echo "ml5CasperDocumentsDocCountBefore=$ml5CasperDocumentsDocCountBefore"

echo  "###################################"
echo  "Killing master cluster"
echo  "###################################"

haltVm ml1

echo  "###################################"
echo  "Adding 2 deliveries"
echo  "###################################"

latestDelivery=`deliverNDeliveryDirs 2 | tail -1`
echo "latestDelivery=$latestDelivery"

echo  "###################################"
echo  "Enable replication from slave1 cluster (as master) to slave2"
echo  "###################################"
./bin/replication enable conf/slave1-slave2.cfg

echo  "###################################"
echo  "Reconfigure Aim to write to slave1 "
echo  "###################################"
aimUpdateMarkLogicUrlHost 'ml3.domain.com'

echo  "###################################"
echo  "Waiting until Aim has picked up new deliveries..."
echo  "###################################"

waitTilAimHasProcessed "$latestDelivery"

echo  "###################################"
echo  "Document counts"
echo  "###################################"
ml3CasperDocumentsDocCountAfter=`getDocCount 'ml3.domain.com' 'CasperDocuments'`
ml5CasperDocumentsDocCountAfter=`getDocCount 'ml5.domain.com' 'CasperDocuments'`
echo "ml3CasperDocumentsDocCountAfter=$ml3CasperDocumentsDocCountAfter"
echo "ml5CasperDocumentsDocCountAfter=$ml5CasperDocumentsDocCountAfter"

echo  "###################################"
echo  "Adding 2 deliveries"
echo  "###################################"

latestDelivery=`deliverNDeliveryDirs 2 | tail -1`
echo "latestDelivery=$latestDelivery"

echo  "###################################"
echo  "Waiting until Aim has picked up new deliveries..."
echo  "###################################"

waitTilAimHasProcessed "$latestDelivery"

echo  "###################################"
echo  "Document counts"
echo  "###################################"
ml3CasperDocumentsDocCountAfter=`getDocCount 'ml3.domain.com' 'CasperDocuments'`
ml5CasperDocumentsDocCountAfter=`getDocCount 'ml5.domain.com' 'CasperDocuments'`
echo "ml3CasperDocumentsDocCountAfter=$ml3CasperDocumentsDocCountAfter"
echo "ml5CasperDocumentsDocCountAfter=$ml5CasperDocumentsDocCountAfter"


