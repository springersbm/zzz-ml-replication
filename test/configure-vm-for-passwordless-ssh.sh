#!/bin/bash

host="${1}"

if [ -z "$host" ]; then
    echo "Usage : $0 <host>"
    exit 1
fi

if [[ `ssh -oBatchMode=yes vagrant@"$host" 'echo true'` == "true" ]]; then
    echo "Passwordless ssh to $host is already working."
    exit
fi

echo "You will be prompted for vagrant's password twice. The password is 'vagrant'"

ssh vagrant@"$host" 'mkdir -p .ssh'
cat ~/.ssh/id_rsa.pub | ssh vagrant@"$host" 'cat >> .ssh/authorized_keys'
ssh vagrant@"$host" 'chmod 700 .ssh && chmod 640 .ssh/authorized_keys'

echo "The aim vm is now configured for password-less ssh. Try it: ssh vagrant@$host"


