#!/bin/bash

BIN_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${BIN_DIR}/test-functions.sh"

echo "getDocCount 'ml1.domain.com' 'CasperDocuments'"
getDocCount 'ml1.domain.com' 'CasperDocuments'
echo "getDocCount 'ml3.domain.com' 'CasperDocuments'"
getDocCount 'ml3.domain.com' 'CasperDocuments'
echo "getDocCount 'ml5.domain.com' 'CasperDocuments'"
getDocCount 'ml5.domain.com' 'CasperDocuments'
