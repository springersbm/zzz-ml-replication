#!/bin/bash

set -e
set -u

BASE_DIR='/home/casper/casper'

echo "##########################################"
echo "1. Reset MarkLogic VMs to unconfigured"
echo "##########################################"

cd "$BASE_DIR"/ansible_playbook_sl_marklogic

#vagrant destroy -f ml1 ml2 ml3 ml4
vagrant up
./clear_mls.sh

echo "##########################################"
echo "2. Install & configure ML"
echo "##########################################"

./run-ansible.sh vagrant-master.yml
./run-ansible.sh vagrant-slave1.yml
./run-ansible.sh vagrant-slave2.yml

echo "##########################################"
echo "3.1. Apply ml-config-sl Steps to ml1.domain.com"
echo "##########################################"
cd "$BASE_DIR"/ml-config-sl
./apply-steps.sh -t ml1.domain.com
echo "##########################################"
echo "3.2. Apply ml-config-sl Steps to ml3.domain.com"
echo "##########################################"
./apply-steps.sh -t ml3.domain.com
echo "##########################################"
echo "3.3. Apply ml-config-sl Steps to ml5.domain.com"
echo "##########################################"
./apply-steps.sh -t ml5.domain.com

echo "##########################################"
echo "4. install aim xquery to ml1.domain.com and ml3.domain.com"
echo "##########################################"
(
cd "$BASE_DIR"/aim2
./sbt package zip
./ops/deploy/marklogic/deploy-modules.sh ./target/aim-xquery-LOCAL.zip ml1.domain.com
./ops/deploy/marklogic/deploy-modules.sh ./target/aim-xquery-LOCAL.zip ml3.domain.com
)

echo "##########################################"
echo "5. configure vms for passwordless ssh"
echo "##########################################"
"$BASE_DIR"/ml-replication/test/configure-vm-for-passwordless-ssh.sh ml1.domain.com
#"$BASE_DIR"/ml-replication/test/configure-vm-for-passwordless-ssh.sh ml2.domain.com
"$BASE_DIR"/ml-replication/test/configure-vm-for-passwordless-ssh.sh ml3.domain.com
#"$BASE_DIR"/ml-replication/test/configure-vm-for-passwordless-ssh.sh ml4.domain.com
"$BASE_DIR"/ml-replication/test/configure-vm-for-passwordless-ssh.sh ml5.domain.com
#"$BASE_DIR"/ml-replication/test/configure-vm-for-passwordless-ssh.sh ml6.domain.com
"$BASE_DIR"/ml-replication/test/configure-vm-for-passwordless-ssh.sh aim.domain.com

echo "##########################################"
echo "########## SETUP COMPLETE ################"
echo "##########################################"

