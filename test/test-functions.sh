#!/bin/bash

function confirmStatusContains() {
    local line="$@"
    local status=`./bin/replication status conf/master-slave1.cfg`
    if [[ "$status" == *${line}* ]]; then
        echo "Success: status contains '$line'."
    else 
        echo "Failed: status doesn't contain '$line' when it should."
        exit 1
    fi
}

function confirmStatusDoesntContain() {
    local line="$@"
    local status=`./bin/replication status conf/master-slave1.cfg`
    if [[ "$status" == *${line}* ]]; then
        echo "Failed: status contains '$line' when it shouldn't."
        exit 1
    else 
        echo "Success: status doesn't contain '$line'"
    fi
}

function confirmDocCountsAreEqual() {
    local mlHost1=$1
    local mlHost2=$2
    local databaseName=$3
    if [ -z "$mlHost1" ] || [ -z "$mlHost2" ] || [ -z "$databaseName" ]; then
        echo "Usage: confirmDocCountsAreEqual MARKLOGIC-HOSTNAME-1 MARKLOGIC-HOSTNAME-2 DATABASE-NAME"
        exit 1
    fi
    mlHost1Count=`getDocCount "$mlHost1" "$databaseName"`
    mlHost2Count=`getDocCount "$mlHost2" "$databaseName"`
    if [[ "$mlHost1Count" == "$mlHost2Count" ]]; then
        echo "true"
    else
        echo "false"
    fi
}

function getDocCount() {
    local mlHost=$1
    local databaseName=$2
    if [ -z "$mlHost" ] || [ -z "$databaseName" ]; then
        echo "Usage: getDocCount MARKLOGIC-HOSTNAME DATABASE-NAME"
        exit 1
    fi
    response=`curl --max-time 3 -fsS --anyauth -u admin:admin "http://${mlHost}:8002/manage/LATEST/databases/${databaseName}?view=counts&format=json"`
    echo "$response" | sed "s/-//g" | jq '.databasecounts.countproperties.documents.value'
}

function deliverNDeliveryDirs() {
    local numDeliveryDirs=$1
    if [[ $numDeliveryDirs =~ ^-?[0-9]+$ ]]; then
        for dir in `ssh vagrant@192.168.24.47 'find /dds/increments-wip -maxdepth 1 -mindepth 1 -type d' | sort | head -$numDeliveryDirs`; do
            echo "$dir" | cut -f4 -d'/'
            ssh vagrant@192.168.24.47 "mv $dir /dds/increments"
        done
    else
        echo "'$numDeliveryDirs' is not a number"
        exit 1
    fi
    aimRestart
}

function resetDeliveryDirs() {
    ssh vagrant@192.168.24.47 'mv /dds/increments/* /dds/increments-wip/'
}

function aimIsRunning() {
    if [[ `aimCoordinatorIsRunning` == "true" ]] && [[ `aimWorkerIsRunning` == "true" ]] ; then
        echo "true"
    else
        echo "false"
    fi
}

function aimRestart() {
    aimStop
    aimStart
}

function aimStart() {
    ssh vagrant@192.168.24.47 '/etc/init.d/aim-coordinator start'  > /dev/null && ssh vagrant@192.168.24.47 '/etc/init.d/aim-worker start' > /dev/null
}

function aimStop() {
    ssh vagrant@192.168.24.47 '/etc/init.d/aim-worker stop'  > /dev/null && ssh vagrant@192.168.24.47 '/etc/init.d/aim-coordinator stop' > /dev/null
}

function aimTailCoordinatorLog() {
    ssh vagrant@192.168.24.47 'tail -200 /var/log/casper/casper-aim-coordinator.log'
}

function aimTailWorkerLog() {
    ssh vagrant@192.168.24.47 'tail -200 /var/log/casper/casper-aim.log'
}

function aimUpdateMarkLogicUrlHost() {
    local mlHost=$1
    ssh vagrant@192.168.24.47 "sed -i.bak 's/marklogic.url=.*/marklogic.url=xcc:\/\/admin:admin@${mlHost}:8400/' /opt/casper/aim.properties"
    aimRestart
}

function aimCoordinatorIsRunning() {
    local status=`ssh vagrant@192.168.24.47 '/etc/init.d/aim-coordinator status'`
    local isRunningStatus='Casper AIM Coordinator Service is running.'
    if [[ "$status" == *${isRunningStatus}* ]]; then
        echo "true"
    else
        echo "false"
    fi
}

function waitTilAimHasProcessed() {
    local delivery=$1
    NEXT_WAIT_TIME=0
    until [[ `aimHasProcessed "$delivery"` == "true" ]] || [ $NEXT_WAIT_TIME -eq 4 ]; do
       sleep $(( NEXT_WAIT_TIME++ ))
    done
    if [[ `aimHasProcessed "$delivery"` == "true" ]]; then
        echo "Aim has processed $delivery"
    else
        echo "Aim has NOT YET processed $delivery"
        exit 1
    fi
}

function aimHasProcessed() {
    local delivery=$1
    local logTail=`aimTailWorkerLog`
    if [[ "$logTail" == *${delivery}* ]]; then
        echo "true"
    else
        echo "false"
    fi
}

function aimWorkerIsRunning() {
    local status=`ssh vagrant@192.168.24.47 '/etc/init.d/aim-worker status'`
    local isRunningStatus='Casper AIM Worker Service is running.'
    if [[ "$status" == *${isRunningStatus}* ]]; then
        echo "true"
    else
        echo "false"
    fi
}

function haltVm() {
    local vmName=$1
    if [ -z "$vmName" ]; then
        echo "Usage: haltVm VM-NAME"
        exit 1
    fi
    (
        cd ../ansible_playbook_sl_marklogic
        vagrant halt "$vmName"
    )
}

function upVm() {
    local vmName=$1
    if [ -z "$vmName" ]; then
        echo "Usage: upVm VM-NAME"
        exit 1
    fi
    (
        cd ../ansible_playbook_sl_marklogic
        vagrant up "$vmName"
    )
}

function vmStatus() {
    (
        cd ../ansible_playbook_sl_marklogic
        vagrant status
    )
}

function makeSlaveNodesUnreachableFromMasterNodes() {
    ssh vagrant@192.168.24.41 'sudo sed -i "s/^192.168.24.43/#192.168.24.43/" /etc/hosts && sudo sed -i "s/^192.168.24.44/#192.168.24.44/" /etc/hosts && sudo sed -i "s/^192.168.24.45/#192.168.24.45/" /etc/hosts && sudo sed -i "s/^192.168.24.46/#192.168.24.46/" /etc/hosts'
    ssh vagrant@192.168.24.42 'sudo sed -i "s/^192.168.24.43/#192.168.24.43/" /etc/hosts && sudo sed -i "s/^192.168.24.44/#192.168.24.44/" /etc/hosts && sudo sed -i "s/^192.168.24.45/#192.168.24.45/" /etc/hosts && sudo sed -i "s/^192.168.24.46/#192.168.24.46/" /etc/hosts'
}

function makeSlaveNodesReachableFromMasterNodes() {
    ssh vagrant@192.168.24.41 'sudo sed -i "s/^#192.168.24.43/192.168.24.43/" /etc/hosts && sudo sed -i "s/^#192.168.24.44/192.168.24.44/" /etc/hosts && sudo sed -i "s/^#192.168.24.45/192.168.24.45/" /etc/hosts && sudo sed -i "s/^#192.168.24.46/192.168.24.46/" /etc/hosts'
    ssh vagrant@192.168.24.42 'sudo sed -i "s/^#192.168.24.43/192.168.24.43/" /etc/hosts && sudo sed -i "s/^#192.168.24.44/192.168.24.44/" /etc/hosts && sudo sed -i "s/^#192.168.24.45/192.168.24.45/" /etc/hosts && sudo sed -i "s/^#192.168.24.46/192.168.24.46/" /etc/hosts'
}

