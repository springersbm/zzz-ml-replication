#!/bin/bash

set -e

# INSTALL JAVA
cd /opt/
sudo wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/7u67-b01/jdk-7u67-linux-x64.tar.gz"
sudo mv jdk-7u67-linux-x64.tar.gz\?AuthParam\=* jdk-7u67-linux-x64.tar.gz
sudo tar xzf jdk-7u67-linux-x64.tar.gz
cd jdk1.7.0_67/
sudo alternatives --install /usr/bin/java java /opt/jdk1.7.0_67/bin/java 2
sudo alternatives --config java
export JAVA_HOME=/opt/jdk1.7.0_67

# INSTALL BEANSTALKD
cd
wget http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
sudo rpm -ivh epel-release-6-8.noarch.rpm
sudo yum --enablerepo=epel install -y beanstalkd
sudo service beanstalkd start

# INSTALL AIM
sudo yum install -y unzip
sudo yum install -y nc
wget http://ci.tools.springer-sbm.com/go/files/aim/1346/build/1/build/artifacts/aim_2.10-0.1346.zip
sudo mkdir -p /opt/casper/aim/0.1346
sudo chmod -R 777 /opt/casper
unzip -d /opt/casper/aim/0.1346 aim_2.10-0.1346.zip
sudo chmod +x /opt/casper/aim/0.1346/*.sh
(cd /opt/casper/aim && ln -s 0.1346 current)

# INSTALL AIM PROPERTIES
scp casper@slnldogo0046:/opt/casper/aim.properties /opt/casper/aim.properties
sed -i "s/marklogic.url=.*/marklogic.url=xcc:\/\/admin:admin@ml1.domain.com:8400/" /opt/casper/aim.properties
sed -i "s/beanstalkd.host=.*/beanstalkd.host=localhost/" /opt/casper/aim.properties
sed -i "s/aim.coordinator.server=.*/aim.coordinator.server=localhost/" /opt/casper/aim.properties
sed -i "s/akamai.pdf.upload=.*/akamai.pdf.upload=false/" /opt/casper/aim.properties

# INSTALL AIM INIT.D SCRIPTS

sudo scp casper@slnldogo0046:/etc/init.d/aim-coordinator /etc/init.d/aim-coordinator

sudo sed -i "s/^export JAVA_HOME.*//" /etc/init.d/aim-coordinator
sudo sed -i "s/-Denv=livea/-Denv=vagrant/" /etc/init.d/aim-coordinator
sudo sed -i "s/CASPER_AIM_USER=casper/CASPER_AIM_USER=vagrant/" /etc/init.d/aim-coordinator
sudo sed -i "s/pgrep -u casper/pgrep -u \${CASPER_AIM_USER}/" /etc/init.d/aim-coordinator

sudo scp casper@slnldogo0046:/etc/init.d/aim-worker /etc/init.d/aim-worker

sudo sed -i "s/^export JAVA_HOME.*//" /etc/init.d/aim-worker
sudo sed -i "s/-Denv=livea/-Denv=vagrant/" /etc/init.d/aim-worker
sudo sed -i "s/CASPER_AIM_USER=casper/CASPER_AIM_USER=vagrant/" /etc/init.d/aim-worker
sudo sed -i "s/pgrep -u casper/pgrep -u \${CASPER_AIM_USER}/" /etc/init.d/aim-worker


# INSTALL AIM XQUERY TO REMOTE MARKLOGIC
(cd /opt/casper/aim/0.1346/marklogic/deploy && bash ./deploy-modules.sh aim-xquery-0.1346.zip ml1.domain.com)

# CREATE DIRS

sudo mkdir -p /dds/increments
sudo mkdir /dds/receipts
sudo chown -R vagrant:vagrant /dds

sudo mkdir -p /static
sudo chown -R vagrant:vagrant /static

sudo mkdir -p /var/log/casper
sudo chown -R vagrant:vagrant /var/log/casper 


