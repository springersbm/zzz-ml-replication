#! /usr/bin/env python

import argparse
import os
import sys
import json
from marklogic import MarkLogic

class MarkLogicReplicationAdmin(object):
    def __init__(self):
        parser = argparse.ArgumentParser(description='Configures MarkLogic replication',
                                         usage='''replication_admin <command> [<args>]

Commands are:

   clusters             Get clusters known to a host
   cluster_couple       Couple 2 clusters
   cluster_decouple     De-couple 2 clusters

   db_replication_status   Get database replication status
   db_replication_enable   Enable database replication between 2 clusters
   db_replication_disable  Disable database replication between 2 clusters

''')
        parser.add_argument('command', help='Command to run')
        args = parser.parse_args(sys.argv[1:2])
        if not hasattr(self, args.command):
            print('Unrecognized command')
            parser.print_help()
            exit(1)
        getattr(self, args.command)()

    def clusters(self):
        parser = argparse.ArgumentParser(description='Get clusters known to an ML server')
        parser.add_argument('--host', required=True, help='Hostname of server.')
        parser.add_argument('--username', required=True, help='Username for ML server')
        parser.add_argument('--password', required=True, help='Password for ML server')
        args = parser.parse_args(sys.argv[2:])
        print json.dumps(MarkLogic(args.master_host, args.master_username, args.master_password).clusters(), indent=4)

    def cluster_couple(self):
        parser = argparse.ArgumentParser(description='Couple 2 clusters')
        parser.add_argument('--master_host', required=True, help='Hostname of server in master cluster.')
        parser.add_argument('--master_username', required=True, help='Username')
        parser.add_argument('--master_password', required=True, help='Password')
        parser.add_argument('--replica_host', required=True, help='Hostname of server in replica cluster.')
        parser.add_argument('--replica_username', required=True, help='Username')
        parser.add_argument('--replica_password', required=True, help='Password')
        args = parser.parse_args(sys.argv[2:])
        master_ml = MarkLogic(args.master_host, args.master_username, args.master_password)
        replica_ml = MarkLogic(args.replica_host, args.replica_username, args.replica_password)
        master_info = master_ml.get_cluster_properties()
        replica_info = replica_ml.get_cluster_properties()
        master_ml.add_cluster(replica_info)
        replica_ml.add_cluster(master_info)

    def cluster_decouple(self):
        parser = argparse.ArgumentParser(description='Decouple 2 clusters')
        parser.add_argument('--master_host', required=True, help='Hostname of server in master cluster.')
        parser.add_argument('--master_username', required=True, help='Username')
        parser.add_argument('--master_password', required=True, help='Password')
        parser.add_argument('--replica_host', required=True, help='Hostname of server in replica cluster.')
        parser.add_argument('--replica_username', required=True, help='Username')
        parser.add_argument('--replica_password', required=True, help='Password')
        args = parser.parse_args(sys.argv[2:])
        master_ml = MarkLogic(args.master_host, args.master_username, args.master_password)
        master_ml.delete_cluster(args.replica_cluster)
        replica_ml = MarkLogic(args.replica_host, args.replica_username, args.replica_password)
        replica_ml.delete_cluster(args.master_cluster)

    def db_replication_enable(self):
        parser = argparse.ArgumentParser(description='Enable db replication between 2 clusters')
        parser.add_argument('--master_host', required=True, help='Hostname of server in master cluster.')
        parser.add_argument('--master_username', required=True, help='Username')
        parser.add_argument('--master_password', required=True, help='Password')
        parser.add_argument('--replica_host', required=True, help='Hostname of server in replica cluster.')
        parser.add_argument('--replica_username', required=True, help='Username')
        parser.add_argument('--replica_password', required=True, help='Password')
        parser.add_argument('--database', required=True, help='Database')
        args = parser.parse_args(sys.argv[2:])
        master_ml = MarkLogic(args.master_host, args.master_username, args.master_password)
        replica_ml = MarkLogic(args.replica_host, args.replica_username, args.replica_password)
        replica_cluster = replica_ml.local_cluster()
        master_cluster = master_ml.local_cluster()
        master_ml.set_replica(replica_cluster, args.database)
        replica_ml.set_master(master_cluster, args.database)

    def db_replication_disable(self):
        parser = argparse.ArgumentParser(description='Disable db replication between 2 clusters')
        parser.add_argument('--master_host', required=True, help='Hostname of server in master cluster.')
        parser.add_argument('--master_username', required=True, help='Username')
        parser.add_argument('--master_password', required=True, help='Password')
        parser.add_argument('--replica_host', required=True, help='Hostname of server in replica cluster.')
        parser.add_argument('--replica_username', required=True, help='Username')
        parser.add_argument('--replica_password', required=True, help='Password')
        parser.add_argument('--database', required=True, help='Database')
        args = parser.parse_args(sys.argv[2:])
        master_ml = MarkLogic(args.master_host, args.master_username, args.master_password)
        replica_ml = MarkLogic(args.replica_host, args.replica_username, args.replica_password)
        master_ml.remove_replica(args.database)
        replica_ml.remove_master(args.database)

    def db_replication_status(self):
        parser = argparse.ArgumentParser(description='Get db replication status')
        parser.add_argument('--master_host', required=True, help='Hostname of server in master cluster.')
        parser.add_argument('--master_username', required=True, help='Username')
        parser.add_argument('--master_password', required=True, help='Password')
        parser.add_argument('--database', required=True, help='Database')
        args = parser.parse_args(sys.argv[2:])
        master_ml = MarkLogic(args.master_host, args.master_username, args.master_password)
        print json.dumps( master_ml.replication_status(args.database), indent=4)

if __name__ == '__main__':
    MarkLogicReplicationAdmin()
